@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-sm-12 col-xs-12">
				<div class="news-post-holder">
					@foreach($news as $item)
					<div class="col-lg-6 col-sm-6 col-xs-12">
						<div class="news-post-widget">
							<img class="img-responsive" src="/home/images/{{$item->image}}" alt="" style="width:100%;height:250px">
							<div class="news-post-detail">
								<span class="date">20 march 2016</span>
								<h2><a href="/news-detail/id-{{$item->id}}">{{$item->name}}</a></h2>
								<p>{{$item->summary}}</p>
							</div>
						</div>
                    </div>
                    @endforeach
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="content-widget top-story" style="background: url(images/top-story-bg.jpg);">
					<div class="top-stroy-header">
						<h2>Top Soccer Headlines Story <a href="#" class="fa fa-fa fa-angle-right"></a></h2>
						<span class="date">July 05, 2017</span>
						<h2>Other Headlines</h2>
					</div>
					<ul class="other-stroies">
						<li><a href="#">Wenger Vardy won't start</a></li>
						<li><a href="#">Evans: Vardy just</a></li>
						<li><a href="#">Pires and Murray </a></li>
						<li><a href="#">Okazaki backing</a></li>
						<li><a href="#">Wolfsburg's Rodriguez</a></li>
						<li><a href="#">Jamie Vardy compared</a></li>
						<li><a href="#">Arsenal target Mkhitaryan</a></li>
						<li><a href="#">Messi wins libel case.</a></li>
					</ul>
				</div>
				<aside id="sidebar" class="right-bar">
					<div class="banner">
						<img class="img-responsive" src="images/adds-3.jpg" alt="#">
					</div>
				</aside>
			</div>
        </div>
        <div class="row">
            <div class="col-lg-9 col-sm-12 col-xs-12"> 
                <div class="news-post-holder">   
                    {{$news->links()}}
                </div>
            </div> 
        </div>
	</div>
</section>
@endsection