@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant">
	<div class="container">
		<div class="row">
			<div class="col-md-12 a sticky">
				<a href="/{{$slug}}/lich-thi-dau"><span class="b">LỊCH THI ĐẤU</span></a>
				<a href="/{{$slug}}/ket-qua-thi-dau"><span class="b">KẾT QUẢ THI ĐẤU</span></a>
				<a href="/{{$slug}}/bang-xep-hang"><span class="b">BẢNG XẾP HẠNG</span></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="chon">
					@foreach($rounds as $item)
					<div class="box">
						<a class="selectRound" round-id={{$item->id}} >{{$item->display}}</a>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-9 Match">
				<h2 style="text-transform: uppercase;">kết quả THI ĐẤU {{$round_name}}</h2>
				<table class="table table-borderless table-dark">
					<tbody>
						@foreach($matches as $item)
						<tr>
							<th scope="row">{{ $item->play_at }}</th>
							<td>
								<h4>{{ $item->team1->name }}</h4>
							</td>
							<td>{{ $item['score1'] }} - {{ $item['score2'] }}</td>
							<td>
								<h4>{{ $item->team2->name }}</h4>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
@endsection
@push('homeJs')
<script type="text/javascript">
 
	$(".box").click(function (e) {
	

	   var ele = $(this).children();
	  
		$.ajax({
		   url: '{{ route('ketQuaThiDau',$slug) }}',
		   type: "post",
		   data: { _token: '{{csrf_token()}}',id: ele.attr("round-id")},
		   success: function (data) {
			$('.Match').html(data);
		   }
		});
	});
	
	$(".chon").on('click', '.box', function () {
		$(".box.active-r").removeClass("active-r");
		$(this).addClass("active-r");
	});
</script>
@endpush