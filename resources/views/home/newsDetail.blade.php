@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant main-heading team">
	<div class="row">
		<div class="container">
			<div class="col-md-9">
				<div class="feature-post">
					<div class="feature-img">
						<img src="/home/images/{{$news[0]['image']}}" class="img-responsive" alt="#" style="width:100%" />
					</div>
					<div class="feature-cont">
						<div class="post-heading">
							{{$news[0]['content']}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection