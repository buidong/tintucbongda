@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<h4>CÁC TRẬN KINH ĐIỂN</h4>
				<aside id="sidebar" class="left-bar">
					<div class="feature-matchs">
						<div class="team-btw-match">
                        @foreach ($hotMatch as $item)
							<ul>
								<li>
                                    <img style="width:60px;height:50px" src="home/images/{{$item->team1->signal}}" alt="">
									<span>{{$item->team1->name}}</span>
								</li>
								<li class="vs"><span>{{$item->play}}</span></li>
								<li>
                                <img style="width:60px;height:50px" src="home/images/{{$item->team2->signal}}" alt="">
									<span style="font-size:14px">{{$item->team2->name}}</span>
								</li>
                            </ul>
                        @endforeach
						</div>
					</div>
				</aside>
				<h4>Points Table Premier League</h4>
				<aside id="sidebar" class="left-bar">
					<div class="feature-matchs">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Team</th>
									<th>P</th>
									<th>W</th>
									<th>GD</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($pointTable as $key => $item)
								<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $item->name }}</td>
									<td>{{ $item->point }}</td>
									<td>{{ $item->win }}</td>
									<td>{{ $item->gd }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</aside>
            </div>
            
            <div class="col-lg-9 col-sm-12 col-xs-12">
				<div class="news-post-holder">
					@foreach($newHot as $item)
					<div class="col-lg-6 col-sm-6 col-xs-12">
						<div class="news-post-widget">
							<img class="img-responsive" src="/home/images/{{$item->image}}" alt="" style="width:100%;height:250px">
							<div class="news-post-detail">
								<h2><a href="/news-detail/id-{{$item->id}}">{{$item->name}}</a></h2>
								<p>{{ $item->created_date }}</p>
								<p>{{$item->summary}}</p>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
            
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12"></div>
            <div class="col-lg-9 col-sm-12 col-xs-12">
                {{$newHot->links()}}
            </div>
		</div>
		
	</div>
</section>
@endsection