<!DOCTYPE html>
<html lang="en">
	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Site Metas -->
	<base href="{{ asset('') }}">
	<title>Game Info</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
        .chon {
            overflow-y: auto;
            width: 200px;
            height: 500px;
            padding: 0;
        }
        
        .box {
            width: 170px;
            height: 50px;
            background-color: #333;
            margin: 0;
            text-align: center;
            line-height: 50px;
			border-bottom: 1px solid #fff
        }
		.active-r, .box:hover {
			background-color: #123456;
			color: #333
		}
		.selectRound {
			cursor: pointer;
			color:#ffffff;
		}
    </style>
<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
	<!-- Site Icons -->
	<link rel="shortcut icon" href="" type="home/image/x-icon" />
	<link rel="apple-touch-icon" href="">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="home/css/bootstrap.min.css">
	<!-- Site CSS -->
	<link rel="stylesheet" href="home/style.css">
	<!-- Colors CSS -->
	<link rel="stylesheet" href="home/css/colors.css">
	<!-- ALL VERSION CSS -->	
	<link rel="stylesheet" href="home/css/versions.css">
	<!-- Responsive CSS -->
	<link rel="stylesheet" href="home/css/responsive.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="home/css/custom.css">
	<!-- font family -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- end font family -->
	<link rel="stylesheet" href="home/css/3dslider.css" />
	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<script src="home/js/3dslider.js"></script>
	</head>
	<body class="game_info" data-spy="scroll" data-target=".header">
	
		<section id="top">
			<header>
				<div class="container">
					<div class="header-top">
						<div class="row">
							<div class="col-md-6">
								<div class="full">
									<div class="logo">
										<a href="index.html"><img src="home/images/logo.png" alt="#" /></a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="right_top_section">
									<!-- social icon -->
									<ul class="social-icons pull-left">
										<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
										<li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
									</ul>
									<!-- end social icon -->
									<!-- button section -->
									<ul class="login">
                              <li class="login-modal">
                                 <a href="/login" class="login"><i class="fa fa-user"></i>Login</a>
                              </li>
                             
                           </ul>
                           <!-- end button section -->
								</div>
							</div>
						</div>
					</div>
					<div class="header-bottom">
						<div class="row">
							<div class="col-md-12">
								<div class="full">
									<div class="main-menu-section">
										<div class="menu">
											<nav class="navbar navbar-inverse">
												<div class="navbar-header">
													<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													</button>
													<a class="navbar-brand" href="#">Menu</a>
												</div>
												<div class="collapse navbar-collapse js-navbar-collapse">
													<ul class="nav navbar-nav">
														<li class="active"><a href="/">Home</a></li>
                                                        @foreach($league as $item)
                                                        <li><a href="/league/{{$item->slug}}">{{$item->name}}</a></li>
                                                        @endforeach
														<li><a href="/news">NEWS</a></li>
													</ul>
												</div>
												<!-- /.nav-collapse -->
											</nav>
											<div class="search-bar">
												<div id="imaginary_container">
                                                <form action="/search" method="GET">
                                                @csrf
													<div class="input-group stylish-input-group">
														<input type="text" name="key" class="form-control"  placeholder="Search" >
														<span class="input-group-addon">
														    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>  
                                                        </span>
                                                    </div>
                                                </form> 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="inner-page-banner">
				<div class="container">
				</div>
			</div>
		</section>
		@yield('content')
		<div class="team-holder theme-padding">
			<div class="container">
				<div class="main-heading-holder">
				</div>
				<div id="team-slider">
					<div class="container">
						<div class="col-md-3">
							<div class="team-column style-2">
								<img src="images/img-1-1.jpg" alt="">
								<div class="player-name">
									<div class="desination-2">Defender</div>
									<h5>Charles Wheeler</h5>
									<span class="player-number">12</span>
								</div>
								<div class="overlay">
									<div class="team-detail-hover position-center-x">
										<p>Lacus vulputate torquent mollis venenatis quisque suspendisse fermentum primis,</p>
										<ul class="social-icons style-4 style-5">
											<li><a class="facebook" href="#" tabindex="0"><i class="fa fa-facebook"></i></a></li>
											<li><a class="twitter" href="#" tabindex="0"><i class="fa fa-twitter"></i></a></li>
											<li><a class="youtube" href="#" tabindex="0"><i class="fa fa-youtube-play"></i></a></li>
											<li><a class="pinterest" href="#" tabindex="0"><i class="fa fa-pinterest-p"></i></a></li>
										</ul>
										<a class="btn blue-btn" href=" /soccer/team-detail.html" tabindex="0">View Detail</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="team-column style-2">
								<img src="images/img-1-2.jpg" alt="">
								<div class="player-name">
									<div class="desination-2">Defender</div>
									<h5>Charles Wheeler</h5>
									<span class="player-number">12</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="team-column style-2">
								<img src="images/img-1-3.jpg" alt="">
								<div class="player-name">
									<div class="desination-2">Defender</div>
									<h5>Charles Wheeler</h5>
									<span class="player-number">12</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="team-column style-2">
								<img src="images/img-1-4.jpg" alt="">
								<div class="player-name">
									<div class="desination-2">Defender</div>
									<h5>Charles Wheeler</h5>
									<span class="player-number">12</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="full">
							<div class="footer-widget">
								<div class="footer-logo">
									<a href="#"><img src="home/images/footer-logo.png" alt="#" /></a>
								</div>
								<p>Most of our events have hard and easy route choices as we are always keen to encourage new riders.</p>
								<ul class="social-icons style-4 pull-left">
									<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
									<li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="full">
							<div class="footer-widget">
								<h3>Menu</h3>
								<ul class="footer-menu">
									<li><a href="about.html">About Us</a></li>
									<li><a href="team.html">Our Team</a></li>
									<li><a href="news.html">Latest News</a></li>
									<li><a href="matche.html">Recent Matchs</a></li>
									<li><a href="blog.html">Our Blog</a></li>
									<li><a href="contact.html">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="full">
							<div class="footer-widget">
								<h3>Contact us</h3>
								<ul class="address-list">
									<li><i class="fa fa-map-marker"></i> Lorem Ipsum is simply dummy text of the printing..</li>
									<li><i class="fa fa-phone"></i> 123 456 7890</li>
									<li><i style="font-size:20px;top:5px;" class="fa fa-envelope"></i> demo@gmail.com</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="full">
							<div class="contact-footer">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d120615.72236587871!2d73.07890527988283!3d19.140910987164396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1527759905404" width="600" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<p>Copyright © 2018 Distributed by <a href="https://themewagon.com/" target="_blank">ThemeWagon</a></p>
				</div>
			</div>
		</footer>
		<a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
		<!-- ALL JS FILES -->
		<script src="home/js/all.js"></script>
		<!-- ALL PLUGINS -->
		<script src="home/js/custom.js"></script>

		@stack('homeJs')
	</body>
</html>