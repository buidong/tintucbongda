@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant">
	<div class="container">
		<div class="row">
			<div class="col-md-12 a sticky">
			<a href="/{{$slug}}/lich-thi-dau"><span class="b">LỊCH THI ĐẤU</span></a>
			<a href="/{{$slug}}/ket-qua-thi-dau"><span class="b">KẾT QUẢ THI ĐẤU</span></a>
			<a href="/{{$slug}}/bang-xep-hang"><span class="b">BẢNG XẾP HẠNG</span></a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-9 col-sm-12 col-xs-12">
				<div class="news-post-holder">
					@foreach($newsleague as $item)
					<div class="col-lg-6 col-sm-6 col-xs-12">
						<div class="news-post-widget">
							<img class="img-responsive" src="/home/images/{{$item->image}}" alt="" style="width:100%;height:250px">
							<div class="news-post-detail">
								<span class="date">{{ $item->created_date }}</span>
								<h2><a href="/news-detail/id-{{$item->id}}">{{$item->name}}</a></h2>
								<p>{{Str::limit($item->content,100)}}</p>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="content-widget top-story" style="background: url(images/top-story-bg.jpg);">
					<div class="top-stroy-header">
						<h2>Top Soccer Headlines Story <a href="#" class="fa fa-fa fa-angle-right"></a></h2>
						<span class="date">July 05, 2017</span>
						<h2>Other Headlines</h2>
					</div>
					<ul class="other-stroies">
						<li><a href="#">Wenger Vardy won't start</a></li>
						<li><a href="#">Evans: Vardy just</a></li>
						<li><a href="#">Pires and Murray </a></li>
						<li><a href="#">Okazaki backing</a></li>
						<li><a href="#">Wolfsburg's Rodriguez</a></li>
						<li><a href="#">Jamie Vardy compared</a></li>
						<li><a href="#">Arsenal target Mkhitaryan</a></li>
						<li><a href="#">Messi wins libel case.</a></li>
					</ul>
				</div>
				<aside id="sidebar" class="right-bar">
					<div class="banner">
						<img class="img-responsive" src="images/adds-3.jpg" alt="#">
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
@endsection