@extends('home.layouts.master')
@section('content')
<section id="contant" class="contant">
	<div class="container">
		<div class="row">
			<div class="col-md-12 a sticky">
				<a href="/{{$slug}}/lich-thi-dau"><span class="b">LỊCH THI ĐẤU</span></a>
				<a href="/{{$slug}}/ket-qua-thi-dau"><span class="b">KẾT QUẢ THI ĐẤU</span></a>
				<a href="/{{$slug}}/bang-xep-hang"><span class="b">BẢNG XẾP HẠNG</span></a>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12"><h2 style="text-transform: uppercase;">bảng xếp hạng {{$name}}</h2></div>
            <div class="col-md-12">
                <div class="feature-matchs">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Team</th>
                                <th>P</th>
                                <th>W</th>
                                <th>T</th>
                                <th>L</th>
                                <th>GD</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pointtable as $key => $item)
                       
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td><img src="images/img-01_004.png" alt="">{{ $item->name }}</td>
                                <td>{{ $item->point }}</td>
                                <td>{{ $item->win }}</td>
                                <td>{{ $item->tie }}</td>
                                <td>{{ $item->defeat }}</td>
                                <td>{{ $item->gd }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</section>
@endsection