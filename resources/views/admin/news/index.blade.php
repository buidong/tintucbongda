@extends('admin.layouts.master')
@section('title','news')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>News</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">News</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            <a href="{{ route('news.create') }}"><button class="btn btn-primary">Add</button></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Summary</th>
                  <th>Hot</th>
                  <th>Image</th>
                  <th>Content</th>
                  <th>League</th>
                  <th>Delete</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($news as $key => $item)
                <tr>
                  <td>{{ ++$key }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->summary }}</td>
                  <td>{{ $item->hot }}</td>
                  <td><img style="width:70px;height:50px" src="home/images/{{ $item->image }}" alt=""></td>
                  <td>{{ $item->content }}</td>
                  <td>{{ $item->league->name }}</td>
                  <td>
                    <form action="/admin/news/{{ $item->id }}" method="POST">
                        @csrf 
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </form>
                  </td>
                  <td><a href="{{ url('/admin/news/'. $item->id .'/edit') }}"><button class="mt-1 btn btn-primary">edit</button></a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
            {{$news->links()}}
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
   
@endsection