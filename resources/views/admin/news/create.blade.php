@extends('admin.layouts.master')@section('title','news') @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tạo tin tức</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('news.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    {{showErrors($errors, 'name')}}</div>
                                <div class="form-group">
                                    <label for="slug">Summary</label>
                                    <input type="text" class="form-control" id="summary" name="summary" placeholder="Summary">
                                    {{showErrors($errors, 'summary')}}</div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="hot" name="hot">
                                    <label class="form-check-label" for="hot">Hot</label>
                                    {{showErrors($errors, 'hot')}}</div>

                                <div class="form-group">
                                    <label for="image">Chọn hình ảnh</label>

                                    <input type="file" name="image">
                                    {{showErrors($errors, 'image')}}</div>

                                <div class="form-group">
                                    <label>League</label>
                                    <select class="form-control" name="league_id">
                                        @foreach($leagues as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors, 'league_id')}}</div>
                                <div class="card card-outline card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">
                Content
              </h3>
                                        <!-- tools box -->
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                <i class="fas fa-minus"></i></button>
                                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                                <i class="fas fa-times"></i></button>
                                        </div>
                                        <!-- /. tools -->
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body pad">
                                        <div class="mb-3">
                                            <textarea class="textarea" placeholder="Place some text here" name="content" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                            {{showErrors($errors, 'content')}}</div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
@endsection