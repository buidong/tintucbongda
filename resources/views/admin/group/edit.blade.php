@extends('admin.layouts.master')
@section('title','group')
@section('content')
<div class="content-wrapper">
<section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tạo group</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="admin/group/{{ $group->id }}">
              @method('PATCH')
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name',$group->name ?? '') }}" required>
                    {{showErrors($errors, 'name')}}</div>
                  <div class="form-group">
                    <label>Competion</label>
                    <select class="form-control" name="competion_id" required>
                        @foreach($competions as $key => $item)
                        <option value="{{$item->id}}"
                        @if ($item->id == old('competion_id', $group->competion_id))selected="selected"@endif
                        >{{$item->name}}</option>
                        @endforeach
                    </select>
                    {{showErrors($errors, 'competion_id')}}</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </div>
@endsection     