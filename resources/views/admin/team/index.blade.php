@extends('admin.layouts.master')
@section('title','team')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Teams</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Team</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            <a href="{{ route('team.create') }}"><button class="btn btn-primary">Add</button></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Match</th>
                  <th>Win</th>
                  <th>Tie</th>
                  <th>Defeat</th>
                  <th>Point</th>
                  <th>Gd</th>
                  <th>Website</th>
                  <th>Competion</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($teams as $key => $item)
                <tr>
                  <td>{{ ++$key }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->address }}</td>
                  <td>{{ $item->nummatch }}</td>
                  <td>{{ $item->win }}</td>
                  <td>{{ $item->tie }}</td>
                  <td>{{ $item->defeat }}</td>
                  <td>{{ $item->point }}</td>
                  <td>{{ $item->gd }}</td>
                  <td>{{ $item->website }}</td>
                  <td>{{ $item->competion->name }}</td>
                  <td>
                  <form action="/admin/team/{{ $item->id }}" method="POST">
										@csrf 
										@method('DELETE')
										<button type="submit" class="btn btn-primary">Delete</button>
									</form>
                  
                  </td>
                  <td><a href="{{ url('/admin/team/'. $item->id .'/edit') }}"><button class="mt-1 btn btn-primary">edit</button></a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-12">
        {{ $teams->links() }}
        </div>

      </div
    </section>
    <!-- /.content -->
  </div>
   
@endsection