@extends('admin.layouts.master')
@section('title','team') @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tạo team</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="name" >
                                    {{showErrors($errors, 'name')}}</div>
                                <div class="form-group">
                                    <label for="ad">Address</label>
                                    <input type="text" class="form-control" id="ad" name="address" placeholder="address" >
                                    {{showErrors($errors, 'address')}}</div>
                                <div class="form-group">
                                    <label for="web">Website</label>
                                    <input type="text" class="form-control" id="web" name="website" placeholder="website" >
                                    {{showErrors($errors, 'website')}}</div>
                                <div class="form-group">
                                    <label for="match">Match</label>
                                    <input type="text" class="form-control" id="match" name="nummatch" placeholder="match"  >
                                    {{showErrors($errors, 'nummatch')}}</div>
                                <div class="form-group">
                                    <label for="win">Win</label>
                                    <input type="text" class="form-control" id="win" name="win" placeholder="win" >
                                    {{showErrors($errors, 'win')}}</div>
                                <div class="form-group">
                                    <label for="tie">Tie</label>
                                    <input type="text" class="form-control" id="tie" name="tie" placeholder="tie" >
                                    {{showErrors($errors, 'tie')}}</div>
                                <div class="form-group">
                                    <label for="defeat">Defeat</label>
                                    <input type="text" class="form-control" id="defeat" name="defeat" placeholder="defeat" >
                                    {{showErrors($errors, 'defeat')}}</div>
                                <div class="form-group">
                                    <label for="point">Point</label>
                                    <input type="text" class="form-control" id="point" name="point" placeholder="Point" >
                                    {{showErrors($errors, 'point')}}</div>
                                <div class="form-group">
                                    <label for="gd">Gd</label>
                                    <input type="text" class="form-control" id="gd" name="gd" placeholder="Gd" >
                                    {{showErrors($errors, 'gd')}}</div>
								
                                <div class="form-group">
                                    <label>Competion</label>
                                    <select class="form-control" name="competion_id" >
                                        @foreach($competions as $key => $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    {{showErrors($errors, 'competion_id')}}</div>
                                <div class="form-group">
                                    <label>Group</label>
                                    <select class="form-control" name="group_id">
                                        @foreach($groups as $key => $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    {{showErrors($errors, 'group_id')}}</div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
@endsection