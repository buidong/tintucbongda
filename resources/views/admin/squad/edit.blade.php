@extends('admin.layouts.master')
@section('title','squad')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Tạo season</h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<form role="form" method="POST" action="admin/squad/{{ $squad->id }}">
							@method('PATCH')
							@csrf
							<div class="card-body">
								<div class="form-group">
									<label for="pos">Position</label>
									<input type="text" class="form-control" id="pos" name="pos" placeholder="Position">
									{{showErrors($errors, 'pos')}}</div>
								<div class="form-group">
									<label>Team</label>
									<select class="form-control" name="team_id">
										@foreach($teams as $item)
										<option value="{{$item->id}}">{{$item->name}}</option>
										@endforeach
									</select>
									{{showErrors($errors, 'team_id')}}</div>
								<div class="form-group">
									<label>Player</label>
									<select class="form-control" name="player_id">
										@foreach($players as $item)
										<option value="{{$item->id}}">{{$item->name}}</option>
										@endforeach
									</select>
									{{showErrors($errors, 'player_id')}}</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection