@extends('admin.layouts.master')
@section('title','match')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Matches</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Match</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            <a href="{{ route('match.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Play at</th>
                  <th>Play time</th>
                  <th>Round</th>
                  <th>Group</th>
                  <th>Team1</th>
                  <th>Score1</th>
                  <th>Team2</th>
                  <th>Score2</th>
                  <th>Delete</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($matches as $key => $item)
                <tr>
                  <td>{{ ++$key }}</td>
                  <td>{{ $item->play_at }}</td>
                  <td>{{ Carbon\Carbon::parse($item->play_time)->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
                  <td>{{ $item->matchround->name }}</td>
                  <td>{{ $item->matchgroup }}</td>
                  <td>{{ $item->team1->name }}</td>
                  <td>{{ $item->score1 }}</td>
                  <td>{{ $item->team2->name }}</td>
                  <td>{{ $item->score2 }}</td>
                  <td>
                    <form action="/admin/match/{{ $item->id }}" method="POST">
                        @csrf 
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </form>
                  </td>
                  <td><a href="{{ url('/admin/match/'. $item->id .'/edit') }}"><button class="mt-1 btn btn-primary">edit</button></a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
            {{$matches->links()}}
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
   
@endsection