@extends('admin.layouts.master')@section('title','match') @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Sửa Match</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="admin/match/{{ $match->id }}">
                            @method('PATCH') @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="at">Play at</label>
                                    <input type="text" class="form-control" id="at"  name="play_at" placeholder="Play at" value="{{ old('play_at',$match->play_at ?? '') }}">
                                    {{showErrors($errors, 'play_at')}}</div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Play in</span>
                                    </div>
                                    <input type="datetime-local" class="form-control" data-inputmask-alias="datetime" name="play_time">
                                    {{showErrors($errors, 'play_time')}}</div>
                                <div class="form-group">
                                    <label>Round</label>
                                    <select class="form-control" name="round_id">
                                        @foreach($rounds as $item)
                                        <option value="{{$item->id}}" @if ($item->id == old('round_id', $match->round_id))selected="selected"@endif>{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors, 'round_id')}}</div>
                                <div class="form-group">
                                    <label>Group</label>
                                    <select class="form-control" name="group_id">
                                        <option></option>
                                        @foreach($groups as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors, 'group_id')}}</div>
                                <div class="form-group">
                                    <label>Team1</label>
                                    <select class="form-control" name="team1_id">
                                        @foreach($team1 as $item)
                                        <option value="{{$item->id}}" @if ($item->id == old('team1_id', $match->team1_id))selected="selected"@endif>{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors, 'team1_id')}}</div>
                                <div class="form-group">
                                    <label for="sc1">Score1</label>
                                    <input type="text" class="form-control" id="sc1" name="score1" value="{{ old('score1',$match->score1 ?? '') }}">
                                    {{showErrors($errors, 'score1')}}</div>
                                <div class="form-group">
                                    <label>Team2</label>
                                    <select class="form-control" name="team2_id">
                                        @foreach($team2 as $item)
                                        <option value="{{$item->id}}" @if ($item->id == old('team2_id', $match->team2_id))selected="selected"@endif>{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors, 'team2_id')}}</div>
                                <div class="form-group">
                                    <label for="sc2">Score2</label>
                                    <input type="text" class="form-control" id="sc2" name="score2" value="{{ old('score2',$match->score2 ?? '') }}">
                                    {{showErrors($errors, 'score2')}}</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
</form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
@endsection