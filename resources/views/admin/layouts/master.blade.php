<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Admin | @yield('title')</title>
		<base href="{{ asset('') }}">
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="admin/plugins/fontawesome-free/css/all.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="admin/dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="admin/plugins/summernote/summernote-bs4.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/" class="nav-link">Home</a>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                <a href="/logout"><button type="button" class="btn btn-primary">logout</button></a>
                </ul>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="/admin/dashboard" class="brand-link">
                <img src="admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                <span class="brand-text font-weight-light">AdminLTE 3</span>
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                        </div>
                        @isset(auth()->user()->name)
                        <div class="info">
                            <b style="color:White; text-transform: uppercase;">{{auth()->user()->name}}</b>
                        </div>
                        @endisset
                    </div>
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
							<li class="nav-item has-treeview">
                                <a href="/admin/season" class="nav-link">
                                    <p>
                                        Seasons
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/season/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/season" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Seasons</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/league" class="nav-link">
                                    <p>
                                        Leagues
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/league/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/league" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Leagues</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/news" class="nav-link">
                                    <p>
                                        News
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/news/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/news" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>News</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/competion" class="nav-link">
                                    <p>
                                        Competions
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/competion/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/competion" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Competions</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/group" class="nav-link">
                                    <p>
                                        Groups
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/group/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/group" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Groups</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/group" class="nav-link">
                                    <p>
                                        Rounds
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/round/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/round" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Rounds</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/team" class="nav-link">
                                    <p>
                                        Teams
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/team/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/team" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Teams</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/player" class="nav-link">
                                    <p>
                                        Players
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/player/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/player" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Players</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/squad" class="nav-link">
                                    <p>
                                    Squads
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/squad/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/squad" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Squads</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="/admin/match" class="nav-link">
                                    <p>
                                    Matchs
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/match/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/match" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Matchs</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="/admin/schedule" class="nav-link">
                                    <p>
                                    Schedules
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                    
                                        <a href="/admin/schedule/create" class="nav-link"><i class="far fa-circle nav-icon"></i>
                                            <p>Thêm</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    
                                        <a href="/admin/schedule" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                            <p>Schedules</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
   @yield('content')
            <!-- /.content-wrapper -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
  
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->
        <!-- jQuery -->
        <script src="admin/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="admin/dist/js/adminlte.js"></script>
        <!-- OPTIONAL SCRIPTS -->
        <script src="admin/dist/js/demo.js"></script>
        <!-- PAGE PLUGINS -->
        <!-- jQuery Mapael -->
        <script src="admin/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
        <script src="admin/plugins/raphael/raphael.min.js"></script>
        <script src="admin/plugins/jquery-mapael/jquery.mapael.min.js"></script>
        <script src="admin/plugins/jquery-mapael/maps/usa_states.min.js"></script>
        <!-- ChartJS -->
        <script src="admin/plugins/chart.js/Chart.min.js"></script>
        <!-- PAGE SCRIPTS -->
        <script src="admin/dist/js/pages/dashboard2.js"></script>
        <script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<!-- AdminLTE App -->
<script src="admin/dist/js/adminlte.min.js"></script>
<!-- Summernote -->
<script src="admin/plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
    </body>
</html>