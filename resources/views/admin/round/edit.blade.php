@extends('admin.layouts.master')
@section('title','round')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Sửa round</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="admin/round/{{ $round->id }}">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name',$round->name ?? '') }}">
                                    {{showErrors($errors, 'name')}}
                                </div>
                                <div class="form-group">
                                    <label for="display">Display</label>
                                    <input type="text" class="form-control" id="display" name="display" value="{{ old('display',$round->display ?? '') }}">
                                    {{showErrors($errors, 'name')}}
                                </div>
                                <div class="form-group">
                                    <label>Competion</label>
                                    <select class="form-control" name="competion_id">
                                    @foreach($competions as $key => $item)
                                    <option value="{{$item->id}}"
                                    @if ($item->id == old('competion_id', $round->competion_id))selected="selected"@endif
                                    >{{$item->name}}</option>
                                    @endforeach
                                    </select>
                                    {{showErrors($errors, 'competion_id')}}
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="active" name="active" {{ old('active', $round->active) === 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active" >Active</label>
                                    {{showErrors($errors, 'active')}}
								</div>
								<div class="form-check">
                                    <label class="form-check-label" for="stt"  >Stt</label>
                                    <input type="text" class="form-control" id="stt" name="stt" value="{{ old('name',$round->stt ?? '') }}">
                                    {{showErrors($errors, 'stt')}}
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
@endsection