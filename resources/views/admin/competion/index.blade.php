@extends('admin.layouts.master')
@section('title','competion')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Competions</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Competion</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            <a href="{{ route('competion.create') }}"><button class="btn btn-primary">Add</button></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>League</th>
                  <th>Season</th>
                  <th>New</th>
                  <th>Start</th>
                  <th>End</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($competions as $key => $item)
                <tr>
                  <td>{{ ++$key }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->league->name }}</td>
                  <td>{{ $item->season->name }}</td>
                  <td>{{ $item->new }}</td>
                  <td>{{ Carbon\Carbon::parse($item->start_at)->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
                  <td>{{ Carbon\Carbon::parse($item->end_at)->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
                  <td>
                  <form action="/admin/competion/{{ $item->id }}" method="POST">
										@csrf 
										@method('DELETE')
										<button type="submit" class="btn btn-primary">Delete</button>
									</form>
                  </td>
                  <td><a href="{{ url('/admin/competion/'. $item->id .'/edit') }}"><button class="mt-1 btn btn-primary">Edit</button></a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   
@endsection