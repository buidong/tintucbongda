@extends('admin.layouts.master')
@section('title','competion')
 @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tạo competion</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('competion.store') }}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" >
                                    {{showErrors($errors,'name')}}
                                </div>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Start</i></span>
                                    </div>
                                    <input type="date"  class="form-control" data-inputmask-alias="datetime" name="start_at" >
                                    {{showErrors($errors,'start_at')}}
                                </div>
                                <br>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">End</span>
                                    </div>
                                    <input type="date" class="form-control" data-inputmask-alias="datetime" name="end_at" >
                                    {{showErrors($errors,'end_at')}}
                                </div>

                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="new" name="new">
                                    <label class="form-check-label" for="new">New</label>
                                </div>
                                <div class="form-group">
                                    <label>League</label>
                                    <select class="form-control" name="league_id" >
                                        @foreach($leagues as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors,'league_id')}}
                                </div>
                                <div class="form-group">
                                    <label>Season</label>
                                    <select class="form-control" name="season_id">
                                        @foreach($seasons as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                    {{showErrors($errors,'season_id')}}
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
@endsection