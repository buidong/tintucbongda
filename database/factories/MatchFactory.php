<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Match::class, function (Faker $faker) {
    return [
        'play_at' => $faker->dateTime($max = 'now', $timezone = null),
        'play_time' => $faker->dateTime($max = 'now', $timezone = null),
        'score1' => $faker->randomDigit,
        'score2' => $faker->randomDigit,
        'round_id' => App\Round::pluck('id')->random(),
        'group_id' => App\Group::pluck('id')->random(),
        'team1_id' => App\Team::pluck('id')->random(),
        'team2_id' => App\Team::pluck('id')->random(),
    ];
});
