<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'competion_id' => App\Competion::pluck('id')->random(),
        'active' => $faker->boolean,

    ];
});
