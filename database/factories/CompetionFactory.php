<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Competion::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'start_at' => $faker->dateTime($max = 'now', $timezone = null),
        'end_at' => $faker->dateTime($max = 'now', $timezone = null),
        'league_id' => App\League::pluck('id')->random(),
        'season_id' => App\Season::pluck('id')->random(),
    ];
});
