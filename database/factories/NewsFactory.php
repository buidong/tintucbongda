<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'summary' => $faker->text($maxNbChars = 50),
        'hot' => $faker->boolean,
        'image' => $faker->Image,
        'content' => $faker->text($maxNbChars = 250),
        'league_id' => App\League::pluck('id')->random(),
    ];
});
