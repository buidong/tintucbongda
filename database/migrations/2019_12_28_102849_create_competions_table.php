<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->dateTime('start_at');
            $table->dateTime('end_at');
            $table->boolean('new')->default(0);
            $table->unsignedBigInteger('league_id');
            $table->unsignedBigInteger('season_id');

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competions');
    }
}
