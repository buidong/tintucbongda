<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('address');
            $table->text('website');
            $table->integer('nummatch');
            $table->integer('win');
            $table->integer('defeat');
            $table->integer('tie');
            $table->integer('gd');
            $table->text('signal');
            $table->integer('point');
            $table->unsignedBigInteger('competion_id');
            $table->unsignedBigInteger('group_id')->nullable();

            $table->foreign('competion_id')
                    ->references('id')->on('competions')
                    ->onDelete('cascade');
            $table->foreign('group_id')
                    ->references('id')->on('groups')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
