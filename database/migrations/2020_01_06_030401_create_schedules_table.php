<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('play_at');
            $table->dateTime('play_time');
            $table->boolean('hot');
        
            $table->unsignedBigInteger('round_id')->nullable();
            $table->foreign('round_id')
                    ->references('id')->on('rounds')
                    ->onDelete('cascade');

            $table->unsignedBigInteger('group_id')->nullable();
            $table->foreign('group_id')
                    ->references('id')->on('groups')
                    ->onDelete('cascade');
            
            $table->unsignedBigInteger('team1_id');
            $table->foreign('team1_id')
                    ->references('id')->on('teams')
                    ->onDelete('cascade');

            $table->unsignedBigInteger('team2_id');
            $table->foreign('team2_id')
                    ->references('id')->on('teams')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
