<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $fillable = ['id','name','competion_id'];
    public function competion()
    {
        return $this->belongsTo('App\Competion', 'competion_id', 'id');
    }
    public function match()
    {
        return $this->hasMany('App\Match', 'group_id');
    }
    public function matchgroup()
    {
        return $this->hasMany('App\Match', 'group_id');
    }
    public function schedule()
    {
        return $this->hasMany('App\Schedule', 'group_id');
    }
}
