<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['id','name','summary','hot','image','content','league_id'];
    public function league()
    {
        return $this->belongsTo('App\League', 'league_id');
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }
    
}
