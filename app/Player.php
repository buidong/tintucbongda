<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';
    protected $fillable = ['id','name'];
    public function player()
    {
        return $this->hasMany('App\Squad', 'player_id', 'id');
    }
}
