<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\League;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $leagues = League::all();
        // view()->composer('home.layouts.master',function($view){
        //     $leagues = League::all();
        //     $view->with('league',$leagues);
        // });
        View::share('league', $leagues);
    }
}
