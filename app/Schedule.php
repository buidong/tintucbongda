<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Schedule extends Model
{
    protected $table = 'schedules';
    protected $fillable = ['id','play_at','play_time','hot','round_id','group_id','team1_id','team2_id'];

    public function team1()
    {
        return $this->belongsTo('App\Team', 'team1_id');
    }
    public function team2()
    {
        return $this->belongsTo('App\Team', 'team2_id');
    }
    public function round()
    {
        return $this->belongsTo('App\Round', 'round_id');
    }
    public function schedulegroup()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }
    
    public function getPlayAttribute() {
        return Carbon::parse($this->play_time)->locale('vi-VN')->isoFormat('LLLL');
    }
 
}
    