<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Squad extends Model
{
    protected $table = 'squads';
    protected $fillable = ['id','name','player_id','team_id'];
    public function team()
    {
        return $this->belongsto('App\Team', 'team_id', 'id');
    }
    public function player()
    {
        return $this->belongsto('App\Player', 'player_id', 'id');
    }
}
