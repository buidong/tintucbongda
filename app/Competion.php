<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competion extends Model
{
    protected $table = 'competions';
    protected $fillable = ['name',
       'start_at', 'end_at','league_id','season_id','new'
    ];
 
    public function league()
    {
        return $this->belongsTo('App\League', 'league_id', 'id');
    }
    public function season()
    {
        return $this->belongsTo('App\Season', 'season_id', 'id');
    }
    public function group()
    {
        return $this->hasMany('App\Group', 'competion_id', 'id');
    }
    public function round()
    {
        return $this->hasMany('App\Round', 'competion_id', 'id');
    }
    public function team()
    {
        return $this->hasMany('App\Team', 'competion_id', 'id');
    }
}
