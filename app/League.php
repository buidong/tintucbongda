<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str; 
class League extends Model
{
    protected $table = 'leagues';
    protected $fillable = ['id','slug',
       'name'
    ];
    public function competion()
    {
        return $this->hasMany('App\Competion', 'league_id', 'id');
    }
    public function news()
    {
        return $this->hasMany('App\News', 'league_id');
    }
    public function getSlug($name) {
        
        return Str::slug($name);
    }
}
