<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = ['name','id','address', 'website','nummatch','win','defeat','tie','signal','point','competion_id','group_id'];
    public function schedule1()
    {
        return $this->hasMany('App\Schedule', 'team1_id');
    }
    public function schedule2()
    {
        return $this->hasMany('App\Schedule', 'team2_id');
    }
    public function competion()
    {
        return $this->belongsTo('App\Team', 'competion_id');
    }
    public function squad()
    {
        return $this->hasMany('App\Squad', 'team_id');
    }
    public function match()
    {
        return $this->hasMany('App\Match', 'team_id');
    }
}
