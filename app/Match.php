<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'matchs';
    protected $fillable = ['id','play_at','play_time','score1','score2','round_id','group_id','team1_id','team2_id'];
    public function team1()
    {
        return $this->belongsTo('App\Team', 'team1_id');
    }
    public function team2()
    {
        return $this->belongsTo('App\Team', 'team2_id');
    }
    public function matchround()
    {
        return $this->belongsTo('App\Round', 'round_id');
    }
    public function matchgroup()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }
}
