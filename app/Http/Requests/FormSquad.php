<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormSquad extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "pos" => "required",
            "player_id" => "required",
            "team_id" => "required",
        ];
        
    }
    public function messages(){
        return [
            "pos.required"=>"Trường này không được để trống",
            "player_id.required"=>"Trường này không được để trống",
            "team_id.required"=>"Trường này không được để trống",
        ];
    }
}
