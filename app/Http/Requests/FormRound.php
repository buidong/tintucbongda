<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormRound extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            "name" => "required",
            "display" => "required",
            "competion_id" => "required",
            "stt" => "required",
        ];
    }
    public function messages(){
        return [
            "name.required" => "Trường này không được để trống",
            "competion_id.required" => "Trường này không được để trống",
            "display.required" => "Trường này không được để trống",
            "stt.required" => "Trường này không được để trống",
        ];
    }
}
