<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormCompetion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "start_at" => "required",
            "end_at" => "required|date|after:start_at",
            "league_id" => "required",
            "season_id" => "required",
        ];
    }
    public function messages(){
        return [
            "name.required" => "Trường này không được để trống",
            "start_at.required" => "Trường này không được để trống",
            "end_at.required" => "Trường này không được để trống",
            "end_at.after" => "Ngày kết thúc phải sau ngày bắt đầu",
            "league_id.required" => "Trường này không được để trống",
            "season_id.required" => "Trường này không được để trống",
        ];
    }
}
