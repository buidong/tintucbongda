<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormNews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "summary" => "required",
            "image" => "required",
            "content" => "required",
            "league_id" => "required",
        ];
    }
    public function messages(){
        return [
            "name.required" => "Trường này không được để trống",
            "summary.required" => "Trường này không được để trống",
            "image.required" => "Trường này không được để trống",
            "content.required" => "Trường này không được để trống",
            "league_id.required" => "Trường này không được để trống",
        ];
    }
}
