<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "address" => "required",
            "website" => "required",
            "nummatch" => "required|numeric",
            "win" => "required|numeric",
            "defeat" => "required|numeric",
            "tie" => "required|numeric",
            "point" => "required|numeric",
            "competion_id" => "required",
            "gd" => "required|numeric",
        ];
        
    }
    public function messages(){
        return [
            "name.required"=>"Trường này không được trống",
            "address.required"=>"Trường này không được trống",
            "website.required"=>"Trường này không được trống",
            "nummatch.required"=>"Trường này không được trống",
            "win.required"=>"Trường này không được trống",
            "defeat.required"=>"Trường này không được trống",
            "tie.required"=>"Trường này không được trống",
            "point.required"=>"Trường này không được trống",
            "competion_id.required"=>"Trường này không được trống",
            "gd.required"=>"Trường này không được trống",

            "nummatch.numeric"=>"Trường này phải là số",
            "win.numeric"=>"Trường này phải là số",
            "defeat.numeric"=>"Trường này phải là số",
            "tie.numeric"=>"Trường này phải là số",
            "point.numeric"=>"Trường này phải là số",
            "competion_id.numeric"=>"Trường này phải là số",
            "gd.numeric"=>"Trường này phải là số",
        ];
    }
}
