<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormSchedule extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "play_at" => "required",
            "play_time" => "required",
            "round_id" => "required",
            "team1_id" => "required|different:team2_id",
            "team2_id" => "required|different:team1_id",
        ];
    }
    public function messages(){
        return [
            "play_at.required" => "Trường này không được để trống",
            "play_time.required" => "Trường này không được để trống",
            "round_id.required" => "Trường này không được để trống",
            "team1_id.required" => "Trường này không được để trống",
            "team2_id.required" => "Trường này không được để trống",
            "team1_id.different" => "Trường này không được giống team2",
            "team2_id.different" => "Trường này không được giống team1",
        ];
    }
}
