<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\News;
use App\League;
use App\Http\Requests\FormNews;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate(4);
        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagues = League::all();
        return view('admin.news.create',compact('leagues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormNews $request)
    {
        $data = new News;
        $data->name = $request->name;
        $data->summary = $request->summary;
        $data->hot = $request->has('hot');
        $data->league_id = $request->league_id;
        $data->content = $request->content;

        $image = $request->image;
        $imageName = $image->getClientOriginalName();
        $data->image = $imageName;
        $path = $image->move('home/images', $imageName);

        $data->save();
        return redirect('/admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leagues = League::all();
        $news = News::findOrFail($id);
        return view('admin.news.edit', compact('news','leagues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormNews $request, $id)
    {
        $data = [];
        $news = News::findOrFail($id);

        $data['name'] = $request->name;
        $data['summary'] = $request->summary;
        $data['hot'] = $request->has('hot');
        $data['league_id'] = $request->league_id;
        $data['content'] = $request->content;
        
        $data['image'] = $request->image;
   

   
        if ($news->update($data)) {
            return redirect('/admin/news');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id)->delete();
        return redirect('/admin/news');
    }
}
