<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormMatch;
use App\Match;
use App\Round;
use App\Group;
use App\Team;


class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = Match::paginate(15);
        return view('admin.match.index',compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rounds = Round::all();
        $team1 = Team::all();
        $team2 = Team::all();
        $groups = Group::all();
        return view('admin.match.create',compact('rounds', 'groups','team1','team2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormMatch $request)
    {
        
        $data = new Match;
        $data->play_at = $request->play_at;
        $data->play_time = $request->play_time;
        $data->round_id = $request->round_id;
        if($data->group_id)
        $data->group_id = $request->group_id;
        else $data->group_id = null;
        $data->team1_id = $request->team1_id;
        $data->team2_id = $request->team2_id;
        $data->score2 = $request->score2;
        $data->score1 = $request->score1;
        $data->save();
        return redirect('admin/match');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $match = Match::findOrFail($id);
        $rounds = Round::all();
        $team1 = Team::all();
        $team2 = Team::all();
        $groups = Group::all();
        return view('admin.match.edit',compact('match','rounds','team1','team2','groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormMatch $request, $id)
    {
        $data = [];
        $match = Match::findOrFail($id);
        $data['play_at'] = $request->play_at;
        $data['play_time'] = $request->play_time;
        $data['round_id'] = $request->round_id;
        if($request->group_id)
        $data['group_id'] = $request->group_id;
        else $data['group_id'] = null;
        $data['score1'] = $request->score1;
        $data['score2'] = $request->score2;
        $data['team1_id'] = $request->team1_id;
        $data['team2_id'] = $request->team2_id;
       
        if ($match->update($data)) {
            return redirect('/admin/match');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $match = Match::find($id)->delete();
        return redirect('/admin/match');
    }
}
