<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Competion;
use App\Season;
use App\League;
use App\Http\Requests\FormCompetion;

class CompetionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competions = Competion::all();
        return view('admin.competion.index',compact('competions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagues = League::all();
        $seasons = Season::all();
        return view('admin.competion.create',compact('leagues','seasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormCompetion $request)
    {
  
        $data = new Competion;
        $data->name = $request->name;
        if($request->get('new'))
        $data->new = 1;
        else $data->new = 0;
        $data->league_id = $request->league_id;
        $data->season_id = $request->season_id;
        $data->start_at = $request->start_at;
        $data->end_at = $request->end_at;


        $data->save();
        return redirect('/admin/competion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leagues = League::all();
        $seasons = Season::all();
        $competion = Competion::findOrFail($id);
        return view('admin.competion.edit', compact('competion','leagues','seasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormCompetion $request, $id)
    {
      
        $data = [];
        $competion = Competion::findOrFail($id);

     
        $data['name'] = $request->name;
        $data['start_at'] = $request->start_at;
        $data['end_at'] = $request->end_at;
        
        $data['league_id'] = $request->league_id;
        $data['season_id'] = $request->season_id;
        if($request->has('new'))
        {
            $data['new'] = true;
        }
        else $data['new'] = false ;
        



   
        if ($competion->update($data)) {
            return redirect('/admin/competion');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $competion = Competion::find($id)->delete();
        return redirect('/admin/competion');
    }
}
