<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Round;
use App\Competion;
use App\Http\Requests\FormRound;

class RoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rounds = Round::paginate(38);
        return view('admin.round.index',compact('rounds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rounds = Round::all();
        $competions = Competion::all();
        return view('admin.round.create',compact('rounds','competions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormRound $request)
    {
        $data = new Round;
        $data->name = $request->name;
        $data->display = $request->display;
        $data->competion_id = $request->competion_id;
        if($request->get('active'))
        $data->active = 1;
        else 
        $data->active =  0;
        $data->stt =  $request->stt;

        $data->save();
        return redirect('/admin/round');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competions = Competion::all();
        $round = Round::findOrFail($id);
        return view('admin.round.edit', compact('competions','round'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormRound $request, $id)
    {
        $data = [];
        $round = Round::findOrFail($id);
        $data['name'] = $request->name;
        $data['display'] = $request->display;
        $data['competion_id'] = $request->competion_id;
        $data['stt'] =  $request->stt;
        $data['active'] = $request->has('active');
        if ($round->update($data)) {
            return redirect('/admin/round');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $round = Round::find($id)->delete();
        return redirect('/admin/round');
    }
}
