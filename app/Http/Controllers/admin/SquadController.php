<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Squad;
use App\Team;
use App\Player;
use App\Http\Requests\FormSquad;

class SquadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $squads = Squad::all();
        return view('admin.squad.index',compact('squads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $players = Player::all();
        $teams = Team::all();
        return view('admin.squad.create',compact('players','teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormSquad $request)
    {
        $squad = new Squad;
        
        $squad->pos = $request->pos;
        $squad->team_id = $request->team_id;
        $squad->player_id = $request->player_id;
        $squad->save();
        return redirect('/admin/squad');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $players = Player::all();
        $teams = Team::all();
        $squad = Squad::findOrFail($id);
        return view('admin.squad.edit',compact('players','teams','squad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormSquad $request, $id)
    {
        $data = [];
        $squad = Squad::findOrFail($id);
        $data['pos'] = $request->pos;
        $data['player_id'] = $request->player_id;
        $data['team_id'] = $request->team_id;
       

        $squad->update($data);
            return redirect('/admin/squad');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $squad = Squad::find($id)->delete();
        return redirect('/admin/squad');
    }
}
