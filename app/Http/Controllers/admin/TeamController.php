<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\Competion;
use App\Group;
use App\Http\Requests\FormTeam;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::paginate(20);
        return view('admin.team.index',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $competions = Competion::all();
        $groups = Group::all();
        return view('admin.team.create',compact('competions','groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormTeam $request)
    {
        $data = new Team;
        $data->name = $request->name;
        $data->address = $request->address;
        $data->website = $request->website;
        $data->tie = $request->tie;
        $data->point = $request->point;
        $data->win = $request->win;
        $data->defeat = $request->defeat;
        $data->competion_id = $request->competion_id;
        $data->group_id = $request->group_id;
        $data->nummatch = $request->nummatch;
        $data->gd = $request->gd;
        
        $data->signal = '';

        $data->save();
        return redirect('/admin/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competions = Competion::all();
        $groups = Group::all();
        $team = Team::findOrFail($id);
        return view('admin.team.edit',compact('team','groups','competions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormTeam $request, $id)
    {
        $data = [];
        $team = Team::findOrFail($id);
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $data['website'] = $request->website;
        $data['tie'] = $request->tie;
        $data['point'] = $request->point;
        $data['win'] = $request->win;
        $data['defeat'] = $request->defeat;
        $data['competion_id'] = $request->competion_id;
        $data['group_id'] = $request->group_id;
        $data['nummatch'] = $request->nummatch;
        $data['gd'] = $request->gd;
        
        // $image = $request->flimage;
        // $imageName = $image->getClientOriginalName();
        // $data->signal = $imageName;
        // $path = $image->move('home/images', $imageName);
        $data['signal'] = '';

   

   
        if ($team->update($data)) {
            return redirect('/admin/team');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id)->delete();
        return redirect('/admin/team');
    }
}
