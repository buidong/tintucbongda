<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\League;
use App\Http\Requests\Formleague;

class LeagueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = League::all();
        return view('admin.league.index',compact('leagues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.league.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Formleague $request)
    {
        $data = new League;
        $data->name = $request->name;
        $data->slug = $data->getSlug($request->name);

        $data->save();
        return redirect('/admin/league');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = League::select('id', 'name','slug')->get()->toArray();
        $league = League::findOrFail($id);
        return view('admin.league.edit', compact('league', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Formleague $request, $id)
    {
        $league = League::findOrFail($id);
        $data = [];
        $data['name'] = $request->name;
        $data['slug'] = $league->getSlug($request->name);
        

        if ($league->update($data)) {
            return redirect('/admin/league');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $league = League::find($id)->delete();
        return redirect('/admin/league');
    }
}
