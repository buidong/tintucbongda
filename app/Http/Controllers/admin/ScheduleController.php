<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormSchedule;
use App\Schedule;
use App\Team;
use App\Group;
use App\Round;


class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::paginate(10);
        return view('admin.schedule.index',compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rounds = Round::all();
        $team1 = Team::all();
        $team2 = Team::all();
        $groups = Group::all();
        return view('admin.schedule.create',compact('rounds', 'groups','team1','team2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormSchedule $request)
    {
        $data = new Schedule;
        $data->play_at = $request->play_at;
        $data->play_time = $request->play_time;
        $data->round_id = $request->round_id;
        $data->hot = $request->hot;
        if($data->group_id)
        $data->group_id = $request->group_id;
        else $data->group_id = null;
        $data->team1_id = $request->team1_id;
        $data->team2_id = $request->team2_id;
        $data->save();
        return redirect('admin/schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedule::findOrFail($id);
        $rounds = Round::all();
        $team1 = Team::all();
        $team2 = Team::all();
        $groups = Group::all();
        return view('admin.schedule.edit',compact('schedule','rounds','team1','team2','groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormSchedule $request, $id)
    {
        $data = [];
        $schedule = Schedule::findOrFail($id);
        $data['play_at'] = $request->play_at;
        $data['play_time'] = $request->play_time;
        $data['round_id'] = $request->round_id;
         $data['group_id'] = $request->group_id;
        $data['hot'] = $request->hot;
        $data['team1_id'] = $request->team1_id;
        $data['team2_id'] = $request->team2_id;
       
        if ($schedule->update($data)) {
            return redirect('/admin/schedule');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id)->delete();
        return redirect('/admin/schedule');
    }
}
