<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\Competion;
use App\League;
use App\News;
use App\Round;
use App\Schedule;
use App\Match;
use Carbon\Carbon;
class HomeController extends Controller
{
    public function index()

    {
        $hotMatch = Schedule::where('hot',true)->get();
        $newHot = News::where('hot',true)->paginate(6);
        $idLeague = League::where('name','Premier League')->value('id');
        $idCompetion = Competion::where('league_id',$idLeague)->where('new',true)->value('id'); 
        $pointTable = Team::where('competion_id',$idCompetion)->orderBy('point','DESC')->orderBy('gd','DESC')->get();
        
        return view('home.index',compact('pointTable','newHot','hotMatch'));
    }

    public function news()

    {
        $news = News::select('id','name','summary','image','content')->paginate(10);
        return view('home.news',compact('news'));
    }

    public function newsleague($slug)

    {
        $id = League::where('slug',$slug)->value('id');
        $newsleague = News::where('league_id',$id)->get();
        return view('home.newsLeague',compact('newsleague','slug'));
    }

    public function viewNews(Request $request,$id)

    {
        $news = News::where('id',$id)->get();
        return view('home.newsDetail',compact('news'));
    }
    public function search(Request $request)

    {
        $key = $request->key;
        $newsea = News::select('id','name','summary','image','content')->where('name','LIKE', "%{$key}%")->get();
        
        return view('home.newsSearch',compact('newsea'));
    }

    public function scheduleAjax(Request $request) {
            $data='';
            $output = '';
            $round_name = Round::where('id',$request->id)->value('name');
            $schedules = Schedule::where('round_id',$request->id)->get();
            
            


            foreach ($schedules as $key => $schedule) {
            $output .= '<tr>
                            <th scope="row"> '. $schedule->play  .' </th>
                            <td>
                                <h4>'. $schedule->team1->name .'</h4>
                            </td>
                            <td>vs</td>
                            <td>
                                <h4>'. $schedule->team2->name .'</h4>
                            </td>
                        </tr>';
            }
            
            $data .= '<h2 style="text-transform: uppercase;">LỊCH THI ĐẤU '.$round_name.'</h2>
                    <table class="table table-borderless table-dark">
                        <tbody class="Schedule">'. $output . '</tbody>
                    </table>';
          
            
            return response($data);
            
        
    }
    public function schedule($slug)
    {
        $league_id = League::where('slug',$slug)->value('id');
        $competion_id = Competion::where('league_id',$league_id)->where('new',true)->value('id');
        $rounds = Round::where('competion_id',$competion_id)->orderBy('stt','asc')->get();
        $round_id = Round::where('active',true)->where('competion_id',$competion_id)->value('id');
        $schedules = Schedule::where('round_id',$round_id)->get();
        $round_name = Round::where('id',$round_id)->value('name');
        return view('home.schedule',compact('rounds','slug','schedules','round_name'));
        
    }
     
    public function match($slug)
    {
        $league_id = League::where('slug',$slug)->value('id');
        $competion_id = Competion::where('league_id',$league_id)->where('new',true)->value('id');
        $rounds = Round::where('competion_id',$competion_id)->orderBy('stt','asc')->get();

        $round_id = Round::where('active',true)->where('competion_id',$competion_id)->value('id');
        $matches = Match::where('round_id',$round_id)->get();
        $round_name = Round::where('id',$round_id)->value('name');


        return view('home.match',compact('rounds','slug','matches','round_name'));
    }
    public function matchAjax(Request $request)
    {
        $data='';
            $output = '';
            $round_name = Round::where('id',$request->id)->value('name');
            $matches = Match::where('round_id',$request->id)->get();
            
            


            foreach ($matches as $key => $item) {
            $output .= '<tr>
                            <th scope="row">'. $item->play_at .'</th>
                            <td>
                                <h4>'. $item->team1->name .'</h4>
                            </td>
                            <td>'. $item->score1 .'-' . $item->score2 .'</td>
                            <td>
                                <h4>'. $item->team2->name .'</h4>
                            </td>
                        </tr>';
            }
            
            $data .= '<h2 style="text-transform: uppercase;">kết quả THI ĐẤU '.$round_name.'</h2>
                        <table class="table table-borderless table-dark">
                            <tbody>'. $output . '</tbody>
                        </table>';
          
            
            return response($data);
    }


    public function rank($slug)
    {

        $idLeague = League::where('slug',$slug)->value('id');
        $name = League::where('slug',$slug)->value('name');
        $idCompetion = Competion::where('league_id',$idLeague)->where('new',true)->value('id'); 
        $pointtable = Team::where('competion_id',$idCompetion)->orderBy('point','DESC')->orderBy('gd','DESC')->get();

        return view('home.rank',compact('rounds','slug','pointtable','name'));
    }
    
}
