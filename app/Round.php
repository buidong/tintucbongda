<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
    
class Round extends Model
{
    protected $table = 'rounds';
    protected $fillable = ['id','name','active','display','stt'];
    public function schedule()
    {
        return $this->hasMany('App\Schedule', 'round_id');
    }

 
    public function competion()
    {
        return $this->belongsTo('App\Competion', 'competion_id', 'id');
    }
    public function matchround()
    {
        return $this->hasMany('App\Match', 'round_id');
    }
}
