<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table = 'seasons';
    protected $fillable = ['id','name'];
    public function competion()
    {
        return $this->hasMany('App\Competion', 'season_id', 'id');
    }
}
