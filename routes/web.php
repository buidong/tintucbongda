<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace'=>'home'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('/news', 'HomeController@news');
    Route::get('/league/{slug}', 'HomeController@newsleague');
    Route::get('/{slug}/lich-thi-dau', 'HomeController@schedule');
    Route::post('/{slug}/lich-thi-dau', 'HomeController@scheduleAjax')->name('lichThiDau');
    Route::get('/{slug}/ket-qua-thi-dau', 'HomeController@match');
    Route::post('/{slug}/ket-qua-thi-dau', 'HomeController@matchAjax')->name('ketQuaThiDau');
    Route::get('/{slug}/bang-xep-hang', 'HomeController@rank');
    Route::get('news-detail/id-{id}', 'HomeController@viewNews');
    Route::get('/search', 'HomeController@search');
});






Route::group(['prefix'=>'admin','middleware'=>'auth', 'namespace'=>'admin'], function(){
    Route::get('dashboard', 'adminController@dashboard');
    Route::resource('season', 'SeasonController');
    Route::resource('league', 'LeagueController');
    Route::resource('news', 'NewsController');
    Route::resource('competion', 'CompetionController');
    Route::resource('group', 'GroupController');
    Route::resource('round', 'RoundController');
    Route::resource('team', 'TeamController');
    Route::resource('player', 'PlayerController');
    Route::resource('match', 'MatchController');
    Route::resource('schedule', 'ScheduleController');
    Route::resource('squad', 'SquadController');
});



Auth::routes(['register']);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');